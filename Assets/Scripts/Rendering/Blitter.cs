﻿using UnityEngine;

public class Blitter : MonoBehaviour
{
    public int kCellWidth = 8;
    public int kCellHeight = 8;

    public Texture2D source;

    public bool ReadScreen;
    public bool UsePlane;
    public Material material;
    public MeshRenderer plane;
    public int GameWidth;
    public int GameHeight;
    public bool Scroll;

    public int CellsWide { get { return GameWidth / kCellWidth; } }
    public int CellsHigh { get { return GameHeight / kCellHeight; } }

    public bool FillRandomly;

    private float scale;
    private int sourceWidth;
    private int sourceHeight;
    private int targetWidth;
    private int targetHeight;
    private Texture2D target;
    private Color[] pixels;
    private Color[] targetPixels;

    public int CursorX { get; private set; } = 0;
    public int CursorY { get; private set; } = 0;
    private Color fgColor = new Color(.75f, .75f, .75f);
    private Color bgColor = Color.black;

    private Color[] colors =
    {
        new Color(0, 0, 0),
        new Color(0, 0, 0.5f),
        new Color(0, 0.5f, 0),
        new Color(0, 0.5f, 0.5f),
        new Color(0.5f, 0, 0),
        new Color(0.5f, 0, 0.5f),
        new Color(0.5f, 0.5f, 0),
        new Color(0.75f, 0.75f, 0.75f),
        new Color(0.25f, 0.25f, 0.25f),
        new Color(0, 0, 1),
        new Color(0, 1, 0),
        new Color(0, 1, 1),
        new Color(1, 0, 0),
        new Color(1, 0, 1),
        new Color(1, 1, 0),
        new Color(1, 1, 1)
    };

    private void Awake()
    {
        if (ReadScreen)
        {
            targetWidth = Screen.width;
            targetHeight = Screen.height;
        }
        else
        {
            targetWidth = GameWidth;
            targetHeight = GameHeight;
        }

        scale = Mathf.Floor(Mathf.Max(targetWidth / (float)GameWidth, targetHeight / (float)GameHeight));

        target = new Texture2D(GameWidth, GameHeight, TextureFormat.ARGB32, false);
        Color[] colors = new Color[GameWidth * GameHeight];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = new Color(0, 0, 0, 0f);
        }

        target.SetPixels(colors);
        target.anisoLevel = 0;
        target.filterMode = FilterMode.Point; // aha!

        sourceWidth = source.width;
        sourceHeight = source.height;
        pixels = source.GetPixels(0, 0, sourceWidth, sourceHeight);
        targetPixels = new Color[kCellWidth * kCellHeight];

        if (!UsePlane)
        {
            if (plane != null)
            {
                plane.gameObject.SetActive(false);
            }
        }
        else
        {
            material.mainTexture = target;
        }
    }

    private void Update()
    {
        if (FillRandomly)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                return;
            }
            RandomFill();
        }

        // assuming modifications have been made
        // to do: dirty flag!
        target.Apply(false);
    }

    private void RandomFill()
    {
        for (int x = 0; x < GameWidth / kCellWidth; x++)
        {
            for (int y = 0; y < GameHeight / kCellHeight; y++)
            {
                int sourceX = Random.Range(0, sourceWidth / kCellWidth) * kCellWidth;
                int sourceY = Random.Range(0, sourceHeight / kCellHeight) * kCellHeight;

                Color bg = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
                Color fg = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));

                FillCell(x, y, sourceX, sourceY, bg, fg);
            }
        }

        target.Apply(false);
    }

    private void FloodFill(Color color)
    {
        for (int x = 0; x < GameWidth / kCellWidth; x++)
        {
            for (int y = 0; y < GameHeight / kCellHeight; y++)
            {
                var sx = 0;
                var sy = sourceHeight - kCellHeight;
                FillCell(x, y, sx, sy, color, color);
            }
        }

        target.Apply(false);
    }

    public void FillCell(int x, int y, int sourceX, int sourceY, Color bg, Color fg)
    {
        if (x >= 0 && x < CellsWide && y >= 0 && y < CellsHigh)
        {
            for (int row = 0; row < kCellHeight; row++)
            {
                for (int column = 0; column < kCellWidth; column++)
                {
                    int cellX = sourceX;
                    int cellY = sourceY * sourceWidth;
                    int cellIndex = cellY + cellX;

                    Color pixel = pixels[cellIndex + row * sourceWidth + column];

                    // to do: expecting binary alpha, how to handle non-binary?
                    if (pixel.a < 0.5f)
                    {
                        // sometimes colors from file can have rgb with 0 alpha,
                        // so ignore the color that came in
                        pixel = bg;
                        pixel.a = 1; // aha!
                    }
                    else
                    {
                        pixel.r *= fg.r;
                        pixel.g *= fg.g;
                        pixel.b *= fg.b;
                    }
                    targetPixels[row * kCellWidth + column] = pixel;
                }
            }

            // this wants it in block format, so give cells to it one line at a time
            // to do: store each pixel in block format so can just hand it over en masse!
            y = GameHeight / kCellHeight - y - 1;
            target.SetPixels(x * kCellWidth, y * kCellHeight, kCellWidth, kCellHeight, targetPixels);
        }
    }

    private void OnGUI()
    {
        if (!UsePlane)
        {
            GUI.DrawTexture(new Rect(0, 0, targetWidth, targetHeight), target);//, ScaleMode., true);
        }
    }

    public void Clear()
    {
        FloodFill(bgColor);

        CursorX = 0;
        CursorY = 0;
    }

    public void Write(string text)
    {
        for (int i = 0; i < text.Length; i++)
        {
            char c = text[i];

            if (c == '\n')
            {
                CursorX = 0;
                ScrollText();
            }
            else if (c == '\t')
            {
                CursorX += 8;
            }
            else
            {
                DrawChar(CursorX++, CursorY, c);
            }
        }
    }

    public void DrawChar(int x, int y, char c)
    {
        int ord = c;
        // gotta love switching y coordinates
        int sx = (ord % (sourceWidth / kCellWidth)) * kCellWidth;
        int sy = sourceHeight - kCellHeight - (ord / (sourceWidth / kCellWidth)) * kCellHeight;

        FillCell(x, y, sx, sy, bgColor, fgColor);
    }

    public void WriteLine(string text)
    {
        Write(text);

        CursorX = 0;
        ScrollText();
    }

    private void ScrollText()
    {
        if (++CursorY >= CellsHigh)
        {
            CursorY = CellsHigh-1;
            //lines.RemoveAt(0);
            //lines.Add(string.Empty);

            //Redraw();
            MoveUp(8);
        }
    }

    private void MoveUp(int height)
    {
        Color[] pixels = target.GetPixels(0, 0, target.width, target.height - height);
        FloodFill(bgColor);
        target.SetPixels(0, height, target.width, target.height - height, pixels);
    }

    public void Write(string text, int color)
    {
        SetFGColor(color);
        Write(text);
    }

    public void WriteLine(string text, int color)
    {
        SetFGColor(color);
        WriteLine(text);
    }

    public void SetFGColor(int color)
    {
        fgColor = colors[color];
    }

    public void SetBGColor(int color)
    {
        bgColor = colors[color];
    }

    public void GotoXY(int x, int y)
    {
        CursorX = x;
        CursorY = y;
    }
}
