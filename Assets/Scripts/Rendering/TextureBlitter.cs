﻿using UnityEngine;

// to do: does this need to be a MonoBehaviour?
// settings are convenient, but can be sent from elsewhere,
// and client doesn't connect to this but rather a higher-
// level layer that takes care of various implementations

// This class has no concept of cells -- just exposes a way
// to copy pixels from one source to another and have it 
// show up on a texture that this generates
public class TextureBlitter : MonoBehaviour
{
    [Header("Input")]
    public Texture2D SourceTexture;
    public int TargetWidth;
    public int TargetHeight;
    public int GameWidth;
    public int GameHeight;
    public int CellWidth;
    public int CellHeight;

    [Header("Output")]
    public Material TextureMaterial;

    // constants
    public int SourceTexWidth { get; private set; }
    public int SourceTexHeight { get; private set; }

    // state
    private Texture2D targetTexture;
    private Color[] sourcePixels;
    private Color[] targetPixels;

    private void Awake()
    {
        Init(TargetWidth, TargetHeight, GameWidth, GameHeight, CellWidth, CellHeight);
    }

    private void Update()
    {
        Flush();
    }

    public void Init(int targetWidth, int targetHeight, int gameWidth, int gameHeight, int cellWidth, int cellHeight)
    {
        sourcePixels = SourceTexture.GetPixels();
        SourceTexWidth = SourceTexture.width;
        SourceTexHeight = SourceTexture.height;

        targetTexture = new Texture2D(targetWidth, targetHeight, TextureFormat.ARGB32, mipChain: false)
        {
            anisoLevel = 0,
            filterMode = FilterMode.Point
        };
        TextureMaterial.mainTexture = targetTexture;

        // to do: don't want to re-create this every blit,
        // but this confines it to a specific cell size
        // which is a requirement i'm not happy with
        targetPixels = new Color[cellWidth * cellHeight];

        Flush();
    }

    public void FloodFill(Color color)
    {
        for (int x = 0; x < GameWidth / CellWidth; x++)
        {
            for (int y = 0; y < GameHeight / CellHeight; y++)
            {
                Blit(0, 0, CellWidth, CellHeight, x * CellWidth, y * CellHeight, color, color);
            }
        }
    }

    public void RandomFill()
    {
        int cellsWide = SourceTexWidth / CellWidth;
        int cellsHigh = SourceTexHeight / CellHeight;

        for (int x = 0; x < GameWidth / CellWidth; x++)
        {
            for (int y = 0; y < GameHeight / CellHeight; y++)
            {
                int sourceX = Random.Range(0, cellsWide) * CellWidth;
                int sourceY = Random.Range(0, cellsHigh) * CellHeight;

                Color bg = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
                Color fg = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));

                Blit(sourceX, sourceY, CellWidth, CellHeight, x * CellWidth, y * CellHeight, bg, fg);
            }
        }
    }

    // Copy from source to target, using 0,0 as top-left for sanity
    public void Blit(
        int sourceX, int sourceY, int sourceWidth, int sourceHeight,
        int targetX, int targetY, Color bg, Color fg)
    {
        for (int row = 0; row < sourceHeight; row++)
        {
            for (int column = 0; column < sourceWidth; column++)
            {
                int cellX = sourceX;
                int invertedSourceY = SourceTexHeight - sourceHeight - sourceY;
                int cellY = invertedSourceY * SourceTexWidth;
                int cellIndex = cellY + cellX;

                Color pixel = sourcePixels[cellIndex + row * SourceTexWidth + column];

                // to do: expecting binary alpha, how to handle non-binary?
                if (pixel.a < 0.5f)
                {
                    // sometimes colors from file can have rgb with 0 alpha,
                    // so ignore the color that came in
                    pixel = bg;
                    pixel.a = 1; // aha!
                }
                else
                {
                    pixel.r *= fg.r;
                    pixel.g *= fg.g;
                    pixel.b *= fg.b;
                }

                targetPixels[row * sourceWidth + column] = pixel;
            }
        }

        targetTexture.SetPixels(targetX, TargetHeight - CellHeight - targetY, sourceWidth, sourceHeight, targetPixels);
    }

    public void Flush()
    {
        targetTexture.Apply(false);
    }
}