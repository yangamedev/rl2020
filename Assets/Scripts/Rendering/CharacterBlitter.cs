﻿using UnityEngine;

public class CharacterBlitter
{
    public int CellsWide { get { return blitter.TargetWidth / blitter.CellWidth; } }
    public int CellsHigh { get { return blitter.TargetHeight / blitter.CellHeight; } }
    public Color FG { get; set; } = Color.white;
    public Color BG { get; set; } = Color.black;

    private TextureBlitter blitter;

    private int cursorX;
    private int cursorY;

    public CharacterBlitter(TextureBlitter blitter)
    {
        this.blitter = blitter;
    }

    private void DrawCell(int x, int y, int sx, int sy, Color fg, Color bg)
    {
        if (x >= 0 && x < CellsWide && y >= 0 && y < CellsHigh)
        {
            blitter.Blit(sx, sy, blitter.CellWidth, blitter.CellHeight, 
                x * blitter.CellWidth, y * blitter.CellHeight, bg, fg);
        }
    }

    public void Clear()
    {
        blitter.FloodFill(Color.black);
    }

    public void Write(string str, int x, int y)
    {
        cursorX = x;
        cursorY = y;

        foreach (var c in str)
        {
            int ord = c;
            int sx = (ord % (blitter.SourceTexWidth / blitter.CellWidth)) * blitter.CellWidth;
            int sy = (ord / (blitter.SourceTexWidth / blitter.CellWidth)) * blitter.CellHeight;

            DrawCell(x, y, sx, sy, FG, BG);
        }
    }

    public void Write(string str, int x, int y, Color fg, Color bg)
    {
        FG = fg;
        BG = bg;
        Write(str, x, y);
    }
}
