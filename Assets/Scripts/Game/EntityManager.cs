﻿using System.Collections.Generic;
using UnityEngine;

public class EntityManager
{
    private List<Entity> entities = new List<Entity>();
    private CharacterBlitter blitter;

    public EntityManager(CharacterBlitter blitter)
    {
        this.blitter = blitter;
    }

    public void Add(Entity entity)
    {
        entities.Add(entity);
    }

    public void Update()
    {

    }

    public void Render()
    {
        blitter.FG = Color.white;
        blitter.BG = Color.black;

        foreach (var entity in entities)
        {
            blitter.Write(entity.Art, entity.X, entity.Y);
        }
    }

    public void Remove(Entity entity)
    {
        entities.Remove(entity);
    }

    public void Clear()
    {
        entities.Clear();
    }
}