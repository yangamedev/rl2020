﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Top-level entry point for project
/// </summary>
public class Game : MonoBehaviour
{
    private const int kMaxRooms = 20;
    private const int kMinRoomSize = 7;
    private const int kMaxRoomSize = 13;

    [Header("Input")]
    public TextureBlitter TextureBlitter;

    private CharacterBlitter blitter;
    private EntityManager entityMgr;

    // state
    private Entity playerEntity;
    private DungeonMap map;

    // to do: move elsewhere
    private int cameraRadius;

    private void Awake()
    {
        blitter = new CharacterBlitter(TextureBlitter);
        entityMgr = new EntityManager(blitter);

        MapGenerator generator = new MapGenerator(blitter.CellsWide, blitter.CellsHigh, kMaxRooms, kMaxRoomSize, kMinRoomSize);
        map = generator.CreateMap();

        playerEntity = new Entity(map.Rooms[0].Center.X, map.Rooms[0].Center.Y, "@");
        entityMgr.Add(playerEntity);

        cameraRadius = 5;
        map.UpdateCameraFieldOfView(playerEntity.X, playerEntity.Y, cameraRadius);
    }

    private void Start()
    {
        blitter.Clear();

        Render();
    }

    private void Update()
    {
        HandleInput();
    }

    private void Render()
    {
        map.Render(blitter);
        entityMgr.Render();
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MovePlayer(0, -1);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MovePlayer(0, 1);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MovePlayer(-1, 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MovePlayer(1, 0);
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Shutdown();
        }
    }

    private void MovePlayer(int dx, int dy)
    {
        if (dx != 0 || dy != 0)
        {
            int targetX = playerEntity.X + dx;
            int targetY = playerEntity.Y + dy;

            if (map.GetCell(targetX, targetY).IsWalkable)
            {
                // to do: need to sample what's BENEATH this entity i.e. the map,
                // or any other kind of thing that can be walked on/over
                blitter.Write(".", playerEntity.X, playerEntity.Y);

                playerEntity.X += dx;
                playerEntity.Y += dy;

                blitter.Write(playerEntity.Art, playerEntity.X, playerEntity.Y);

                map.UpdateCameraFieldOfView(playerEntity.X, playerEntity.Y, cameraRadius);

                // to do: probably want to render every frame, not just every input
                Render();
            }
        }
    }

    private void Shutdown()
    {
        Debug.LogWarning("Shutting down");
        blitter.Clear();
        enabled = false;
        Destroy(gameObject);
    }
}