﻿using RogueSharp;
using System.Collections.Generic;
using UnityEngine;

public class DungeonMap : Map
{
    public List<Rectangle> Rooms;
    private readonly Color FloorLitFG = new Color(1, 1, 1);
    private readonly Color FloorUnlitFG = new Color(0.5f, 0.5f, 0.5f);
    private readonly Color WallLitFG = new Color(1, 1, 1);
    private readonly Color WallUnlitFG = new Color(0.5f, 0.5f, 0.5f);

    public DungeonMap()
    {
        Rooms = new List<Rectangle>();
    }

    // to do: map shouldn't have to know how to render itself
    public void Render(CharacterBlitter mapBlitter)
    {
        mapBlitter.Clear();
        foreach (Cell cell in GetAllCells())
        {
            SetConsoleSymbolForCell(mapBlitter, cell);
        }
    }

    private void SetConsoleSymbolForCell(CharacterBlitter console, Cell cell)
    {
        // When we haven't explored a cell yet, we don't want to draw anything
        if (!cell.IsExplored)
        {
            return;
        }

        if (IsInFov(cell.X, cell.Y))
        {
            // Choose the symbol to draw based on if the cell is walkable or not
            // '.' for floor and '#' for walls
            if (cell.IsWalkable)
            {
                console.Write(".", cell.X, cell.Y, FloorLitFG, Color.black);
            }
            else
            {
                console.Write("#", cell.X, cell.Y, WallLitFG, Color.black);
            }
        }
        else
        {
            if (cell.IsWalkable)
            {
                console.Write(".", cell.X, cell.Y, FloorUnlitFG, Color.black);
            }
            else
            {
                console.Write("#", cell.X, cell.Y, WallUnlitFG, Color.black);
            }
        }
    }

    public void UpdateCameraFieldOfView(int cameraX, int cameraY, int radius)
    {
        // Compute the field-of-view based on the player's location and awareness
        ComputeFov(cameraX, cameraY, radius, true);

        // Mark all cells in field-of-view as having been explored
        foreach (Cell cell in GetAllCells())
        {
            if (IsInFov(cell.X, cell.Y))
            {
                SetCellProperties(cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, true);
            }
        }
    }
}