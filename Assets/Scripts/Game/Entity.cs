﻿public class Entity
{
    public int X;
    public int Y;
    public string Art;

    public Entity(int x, int y, string art)
    {
        X = x;
        Y = y;
        Art = art;
    }
}
